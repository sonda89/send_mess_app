# frozen_string_literal: true

Rails.application.routes.draw do
  root 'chat_rooms#index'
  get 'users/index'
  devise_for :users
  resources :users
  mount ActionCable.server => '/cable'

  resources :chat_rooms, only: %i[new create show index]
end
